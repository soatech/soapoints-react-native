# Overview #

Native Mobile UI for the SoaPoints API (https://bitbucket.org/soatech/soapoints-mean-api) done in React Native, currently in the style of ES6.

# Installation #

Follow setup instructions for React Native here:

https://facebook.github.io/react-native/docs/getting-started.html#content

All except react-native init.  The project has already been initialized.

# Running #

React Native runs a server and watcher on port 8080 that your app will talk to and bundle/transcode the JavaScript.


```
#!bash

$ npm start
```

Now you can run your app in the XCode or Android emulator.

To run it on an actual device you need to bundle the code and add that file to your project.

This is built to talk to the SoaPoints API, which can be found here: https://bitbucket.org/soatech/soapoints-mean-api
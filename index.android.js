'use strict';

let SoaPointsReactNative = require('./app/SoaPointsNative');

let React = require('react-native');
let {
    AppRegistry
    } = React;

AppRegistry.registerComponent('SoapointsReactNative', () => SoaPointsReactNative);

'use strict';

let _ = require('lodash');
const dispatcherFactory = require('./Dispatcher');
let Actions = require('./enums/Actions');

const soaPointsReactNativeFactory = function ({ React }) {
    const {
        Navigator
        } = React;

    // PropTypes
    const {

        } = React.PropTypes;

    return function SoaPointsReactNative(props) {
        SoaPointsReactNative.propTypes = {};

        // Declare our private variables and methods

        let dispatcher = dispatcherFactory();
        let navigator:Navigator;
        const navigationEvents:Array = [
            Actions.navigation.GOTO_LOGIN,
            Actions.navigation.GOTO_REGISTRATION,
            Actions.navigation.GOTO_SHARED_TASKS,
            Actions.navigation.GOTO_TASK_FORM,
            Actions.navigation.GOTO_FREQUENCY_TYPE_SELECTOR,
            Actions.navigation.RETURN_FREQUENCY_TYPE_SELECTOR,
            Actions.navigation.GO_BACK
        ];
        let authenticatedUser:User;

        let authenticationHandler = function authenticationHandler(payload:Object) {
            authenticatedUser = payload;
        };

        let navigationHandler = function navigationHandler(action:String, payload:Object) {
            let props:Object = payload;

            if (!props) {
                props = {};
            }

            props.dispatcher = dispatcher;

            if (!props.authenticatedUser && authenticatedUser) {
                props.authenticatedUser = authenticatedUser;
            }

            continueNavigationHandler(action, props);
        };

        let continueNavigationHandler = function continueNavigationHandler(action:String, props:Object) {
            switch (action) {
                case Actions.navigation.GO_BACK:
                    navigator.pop();
                    break;
                case Actions.navigation.RETURN_FREQUENCY_TYPE_SELECTOR:
                    navigator.replacePrevious({action: Actions.navigation.GOTO_SHARED_TASKS, props: props});
                    navigator.pop();
                    break;
                default:
                    navigator.push({action: action, props: props});
            }
        };

        let determineScene = function determineScene(route:Object, navigator:Navigator) {
            switch (route.action) {
                case Actions.navigation.GOTO_REGISTRATION:
                    let RegisterView = require('./components/RegisterView')({React});
                    return React.createElement(RegisterView, route.props);
                case Actions.navigation.GOTO_SHARED_TASKS:
                    let SharedTasksView = require('./components/SharedTasksView')({React});
                    return React.createElement(SharedTasksView, route.props);
                case Actions.navigation.GOTO_FREQUENCY_TYPE_SELECTOR:
                    let FrequencyTypeSelector = require('./components/FrequencyTypeSelector')({React});
                    return React.createElement(FrequencyTypeSelector, route.props);
                case Actions.navigation.GOTO_LOGIN:
                default:
                    const LoginView = require('./components/LoginView')({React});
                    return React.createElement(LoginView, route.props);
            }
        };

        // return our public facing interface
        return {
            componentDidMount: function componentDidMount() {
                navigator = this.refs.Navigator;

                _.map(navigationEvents, (action) => {
                    dispatcher.register(
                        action,
                        `SoapointsReactNative-${action}`,
                        (payload) => navigationHandler(action, payload));
                });

                dispatcher.register(
                    Actions.user.LOGIN_SUCCESS,
                    `SoapointsReactNative-${Actions.user.LOGIN_SUCCESS}`,
                    (payload) => authenticationHandler(payload));

                dispatcher.register(
                    Actions.user.REGISTER_SUCCESS,
                    `SoapointsReactNative-${Actions.user.REGISTER_SUCCESS}`,
                    (payload) => authenticationHandler(payload));
            },

            componentWillUnmount: function componentWillUnmount() {
                _.map(navigationEvents, (action) => {
                    dispatcher.unregister(
                        action,
                        `SoapointsReactNative-${action}`);
                });

                dispatcher.unregister(Actions.user.LOGIN_SUCCESS, `SoapointsReactNative-${Actions.user.LOGIN_SUCCESS}`);
                dispatcher.unregister(Actions.user.REGISTER_SUCCESS, `SoapointsReactNative-${Actions.user.REGISTER_SUCCESS}`);


                dispatcher = null;
                navigator = null;
                authenticatedUser = null;
            },

            render: function render() {
                return <Navigator ref="Navigator"
                                  initialRoute={{name: "Login", props: {dispatcher: dispatcher}, action: Actions.navigation.GOTO_LOGIN }}
                                  renderScene={(route, navigator) => determineScene(route, navigator)}>

                </Navigator>;
            }
        };
    };
};

export default soaPointsReactNativeFactory;
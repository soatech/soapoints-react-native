let React = require('react-native');
let { StyleSheet } = React;

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 25,
        marginBottom: 25
    },
    formInput: {
        height: 25,
        width: 300,
        borderWidth: 1,
        borderColor: '#888888'
    },
    spacer: {
        height: 10
    },
    btnAll: {
        height: 25,
        width: 300,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1
    },
    btnAllText: {
        color: "#FFF",
        fontSize: 14
    },
    btnDefault: {
        backgroundColor: "#fff",
        borderColor: "#ccc"
    },
    btnDefaultText: {
        color: "#333"
    },
    btnPrimary: {
        backgroundColor: "#337ab7",
        borderColor: "#2e6da4"
    },
    btnPrimaryText: {
    },
    btnSuccess: {
        backgroundColor: "#5cb85c",
        borderColor: "#4cae4c"
    },
    btnSuccessText: {
    },
    btnInfo: {
        backgroundColor: "#5bc0de",
        borderColor: "#46b8da"
    },
    btnInfoText: {

    },
    btnWarning: {
        backgroundColor: "#f0ad4e",
        borderColor: "#eea236"
    },
    btnWarningText: {
    },
    btnDanger: {
        backgroundColor: "#d9534f",
        borderColor: "#d43f3a"
    },
    btnDangerText: {

    },
    btnLink: {
        borderColor: "transparent",
        backgroundColor: "transparent"
    },
    btnLinkText: {
        color: "#337ab7"
    }
});
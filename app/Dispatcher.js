'use strict';

const _ = require('lodash');

const dispatcherFactory = function () {

    // declare our private variables and methods

    let actions = {};

    let findRegistration = function findRegistration(action:String, uid:String) {
        let listeners = actions[action];

        if (listeners) {
            _.find(listeners, (listener) => {
                return (listener.uid === uid);
            });
        }
    };

    // return our public facing interface
    return {
        // TODO: Would be a good candidate for WeakMap?
        register: function register(action:String, uid:String, callback:Function) {
            if (!actions[action]) {
                actions[action] = [];
            }

            let registration = findRegistration(action, uid);

            if (!registration) {
                actions[action].push({uid: uid, callback: callback});
            }
        },

        unregister: function unregister(action:String, uid:String) {
            let listeners = actions[action];

            listeners = _.reject(listeners, (listener) => {
                return (listener.uid === uid);
            });

            actions[action] = listeners;
        },

        dispatch: function dispatch(action:String, payload:Object) {
            let listeners = actions[action];

            if (listeners) {
                _.map(listeners, (reg) => {
                    reg.callback(payload);
                });
            }
        }
    };
};

export default dispatcherFactory;
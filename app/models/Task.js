'use strict';

const taskFactory = function (spec:Object) {
    // private variables and methods

    // public variables and methods
    // TODO: I don't like that you can't tell what types these properties are
    return Object.create({
        user: User,
        name: String,
        description: String,
        repeatable: false,
        frequencyType: FrequencyType,
        frequencyQuantifier: Number,
        weekDay: String,
        soaPoints: Number,
        startDate: Date,
        endDate: Date,

        setAll: function setAll(obj:Object) {
            if (obj.hasOwnProperty('name')) {
                this.name = obj.name;
            }

            if (obj.hasOwnProperty('description')) {
                this.description = obj.description;
            }

            if (obj.hasOwnProperty('repeatable')) {
                this.repeatable = obj.repeatable;
            }

            if (obj.hasOwnProperty('frequencyType')) {
                this.frequencyType = obj.frequencyType;
            }

            if (obj.hasOwnProperty('frequencyQuantifier')) {
                this.frequencyQuantifier = obj.frequencyQuantifier;
            }

            if (obj.hasOwnProperty('weekDay')) {
                this.weekDay = obj.weekDay;
            }

            if (obj.hasOwnProperty('soaPoints')) {
                this.soaPoints = obj.soaPoints;
            }

            if (obj.hasOwnProperty('startDate')) {
                this.startDate = obj.startDate;
            }

            if (obj.hasOwnProperty('endDate')) {
                this.endDate = obj.endDate;
            }

            return this;
        }
    }).setAll(spec.task);
};

export default taskFactory;
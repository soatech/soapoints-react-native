'use strict';

const userFactory = function (spec:Object) {

    // private variables and methods

    // public variables and methods
    return Object.create({
        firstName: String,
        lastName: String,
        email: String,
        username: String,
        password: String,
        token: String,

        setAll: function setAll(obj:Object) {

            if (!obj) {
                return;
            }

            if (obj.hasOwnProperty('firstName')) {
                this.firstName = obj.firstName;
            }

            if (obj.hasOwnProperty('lastName')) {
                this.lastName = obj.lastName;
            }

            if (obj.hasOwnProperty('username')) {
                this.username = obj.username;
            }

            if (obj.hasOwnProperty('password')) {
                this.password = obj.password;
            }

            if (obj.hasOwnProperty('email')) {
                this.email = obj.email;
            }

            if (obj.hasOwnProperty('token')) {
                this.token = obj.token;
            }

            return this;
        }
    }).setAll(spec.user);
};

export default userFactory;
'use strict';

module.exports = {
    navigation: {
        GOTO_LOGIN: "action_navigation_GOTO_LOGIN",
        GOTO_REGISTRATION: "action_navigation_GOTO_REGISTRATION",
        GOTO_MY_TASKS: "action_navigation_GOTO_MY_TASKS",
        GOTO_SHARED_TASKS: "action_navigation_GOTO_SHARED_TASKS",
        GOTO_TASK_FORM: "action_navigation_GOTO_TASK_FORM",
        GOTO_FREQUENCY_TYPE_SELECTOR: "action_navigation_GOTO_FREQUENCY_TYPE_SELECTOR",
        RETURN_FREQUENCY_TYPE_SELECTOR: "action_navigation_RETURN_FREQUENCY_TYPE_SELECTOR",
        GO_BACK: "action_navigation_GO_BACK"
    },
    user: {
        CREATE: "action_user_CREATE",
        CREATE_SUCCESS: "action_user_CREATE_SUCCESS",
        CREATE_FAILURE: "action_user_CREATE_FAILURE",
        READ: "action_user_READ",
        READ_SUCCESS: "action_user_READ_SUCCESS",
        READ_FAILURE: "action_user_READ_FAILURE",
        UPDATE: "action_user_UPDATE",
        UPDATE_SUCCESS: "action_user_UPDATE_SUCCESS",
        UPDATE_FAILURE: "action_user_UPDATE_FAILURE",
        DELETE: "action_user_DELETE",
        DELETE_SUCCESS: "action_user_DELETE_SUCCESS",
        DELETE_FAILURE: "action_user_DELETE_FAILURE",
        LOGIN: "action_user_LOGIN",
        LOGIN_SUCCESS: "action_user_LOGIN_SUCCESS",
        LOGIN_FAILURE: "action_user_LOGIN_FAILURE",
        REGISTER: "action_user_REGISTER",
        REGISTER_SUCCESS: "action_user_REGISTER_SUCCESS",
        REGISTER_FAILURE: "action_user_REGISTER_FAILURE",
        LIST_TASKS: "action_user_LIST_TASKS",
        LIST_TASKS_SUCCESS: "action_user_LIST_TASKS_SUCCESS",
        LIST_TASKS_FAILURE: "action_user_LIST_TASKS_FAILURE",
        MODEL_UPDATED: "action_user_MODEL_UPDATED"
    },
    task: {
        CREATE: "action_task_CREATE",
        CREATE_SUCCESS: "action_task_CREATE_SUCCESS",
        CREATE_FAILURE: "action_task_CREATE_FAILURE",
        READ: "action_task_READ",
        READ_SUCCESS: "action_task_READ_SUCCESS",
        READ_FAILURE: "action_task_READ_FAILURE",
        UPDATE: "action_task_UPDATE",
        UPDATE_SUCCESS: "action_task_UPDATE_SUCCESS",
        UPDATE_FAILURE: "action_task_UPDATE_FAILURE",
        DELETE: "action_task_DELETE",
        DELETE_SUCCESS: "action_task_DELETE_SUCCESS",
        LIST_SHARED: "action_task_LIST_SHARED",
        LIST_SHARED_SUCCESS: "action_task_LIST_SHARED_SUCCESS",
        LIST_SHARED_FAILURE: "action_task_LIST_SHARED_FAILURE",
        MODEL_UPDATED: "action_task_MODEL_UPDATED"
    }
};
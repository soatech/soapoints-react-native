'use strict';

module.exports = {
    buildError: function buildError(payload:Object) {
        return (payload.hasOwnProperty('error') ? payload.error : "System Error");
    }
};
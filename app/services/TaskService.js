'use strict';

const objectAssign = require('object-assign');
const baseServiceFactory = require('../services/BaseService');
let taskActions = require('../enums/Actions').task;
let HttpMethods = require('../enums/HttpMethods');
const FrequencyTypes = require('../enums/FrequencyTypes');

const taskServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);

    return objectAssign(baseService, {
        listSharedTasks: function listSharedTasks(frequencyType:String) {
            let append = '/sharedtasks';


            // TODO: Verify this is legit option first
            // compare against enum set
            if (frequencyType && frequencyType.length) {
                append += `/${frequencyType}`;
            }

            if (FrequencyTypes.contains(frequencyType)) {
                console.log("CONTAINS!");
            }

            fetch(this.buildUri(append), this.buildOptions(HttpMethods.GET))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        this.getDispatcher().dispatch(taskActions.LIST_SHARED_FAILURE, responseData);
                    } else {
                        this.getDispatcher().dispatch(taskActions.LIST_SHARED_SUCCESS, responseData);
                    }
                })
                .catch((error) => this.getDispatcher().dispatch(taskActions.LIST_SHARED_FAILURE, error))
                .done();
        }
    });
};

export default taskServiceFactory;
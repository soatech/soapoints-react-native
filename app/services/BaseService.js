'use strict';

const config = require('../configs/config');

const baseServiceFactory = function (spec) {
    const dispatcher = spec.dispatcher;
    const baseUrl = config.SERVER_BASE_URL;
    let authToken = spec.authToken;
    let baseOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    let BaseService = {
        getAuthToken: function getAuthToken() {
            return authToken;
        },
        setAuthToken: function setAuthToken(newToken:String) {
            authToken = newToken;

            if (authToken && authToken.length) {
                baseOptions.headers['Authorization'] = `JWT ${authToken}`;
            }
        },
        getDispatcher: function getDispatcher() {
            return dispatcher;
        },
        buildUri: function buildUri(append:String) {
            return baseUrl + append;
        },
        buildOptions: function buildOptions(method:String, reqBody:Object) {
            let options = baseOptions;

            options.method = method;
            options.body = JSON.stringify(reqBody);

            console.log(options);

            return options;
        }
    };

    BaseService.setAuthToken(authToken);

    return BaseService;
};

export default baseServiceFactory;
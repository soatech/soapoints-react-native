'use strict';

const userActions = require('../enums/Actions').user;
const HttpMethods = require('../enums/HttpMethods');
const objectAssign = require('object-assign');
const baseServiceFactory = require('../services/BaseService');

const userServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);

    return objectAssign(baseService, {
        login: function login(user:User) {

            let reqBody = {
                username: user.username,
                password: user.password
            };

            fetch(this.buildUri('/auth/login'), this.buildOptions(HttpMethods.POST, reqBody))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        this.getDispatcher().dispatch(userActions.LOGIN_FAILURE, responseData);
                    } else {
                        this.getDispatcher().dispatch(userActions.LOGIN_SUCCESS, responseData)
                    }
                })
                .catch((error) => this.getDispatcher().dispatch(userActions.LOGIN_FAILURE, error))
                .done();
        },
        register(user:User) {
            let reqBody = {
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                username: user.username,
                password: user.password
            };

            fetch(this.buildUri('/auth/register'), this.buildOptions(HttpMethods.POST, reqBody))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        this.getDispatcher().dispatch(userActions.REGISTER_FAILURE, responseData);
                    } else {
                        this.getDispatcher().dispatch(userActions.REGISTER_SUCCESS, responseData)
                    }
                })
                .catch((error) => this.getDispatcher().dispatch(userActions.REGISTER_FAILURE, error))
                .done();
        }
    });
};

export default userServiceFactory;
'use strict';

const buttonInfoFactory = function ({React}) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const ButtonBase = require('./ButtonBase')({React});

    const {
        string,
        func
        } = React.PropTypes;

    return function ButtonInfo(props) {
        ButtonInfo.propTypes = {
            ref: string,
            onPress: func,
            text: string
        };

        return {
            render: function render() {
                return <ButtonBase
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    text={this.props.text}
                    secondaryStyle={CommonStyles.btnInfo}
                    secondaryStyleText={CommonStyles.btnInfoText}/>;

            }
        };
    };
};

export default buttonInfoFactory;
'use strict';

const buttonBaseFactory = function ({ React }) {
    const {
        TouchableOpacity,
        Text
        } = React;

    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const {
        string,
        func,
        object,
        number
        } = React.PropTypes;

    return function ButtonBase(props) {
        ButtonBase.propTypes = {
            ref: string,
            onPress: func,
            secondaryStyle: number,
            secondaryStyleText: number,
            text: string
        };

        return {
            render: function render() {
                return <TouchableOpacity
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    style={[CommonStyles.btnAll, this.props.secondaryStyle]}>

                    <Text style={[CommonStyles.btnAllText, this.props.secondaryStyleText]}>{this.props.text}</Text>
                </TouchableOpacity>;
            }
        };
    };
};

export default buttonBaseFactory;
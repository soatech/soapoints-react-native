'use strict';

const buttonPrimaryFactory = function ({React}) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const ButtonBase = require('./ButtonBase')({React});

    const {
        string,
        func
        } = React.PropTypes;

    return function ButtonPrimary(props) {
        ButtonPrimary.propTypes = {
            ref: string,
            onPress: func,
            text: string
        };

        return {
            render: function render() {
                return <ButtonBase
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    text={this.props.text}
                    secondaryStyle={CommonStyles.btnPrimary}
                    secondaryStyleText={CommonStyles.btnPrimaryText}/>;

            }
        };
    };
};

export default buttonPrimaryFactory;
'use strict';

const buttonDefaultFactory = function ({React}) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const ButtonBase = require('./ButtonBase')({React});

    const {
        string,
        func
        } = React.PropTypes;

    return function ButtonDefault(props) {
        ButtonDefault.propTypes = {
            ref: string,
            onPress: func,
            text: string
        };

        return {
            render: function render() {
                return <ButtonBase
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    text={this.props.text}
                    secondaryStyle={CommonStyles.btnDefault}
                    secondaryStyleText={CommonStyles.btnDefaultText}/>;

            }
        };
    };
};

export default buttonDefaultFactory;
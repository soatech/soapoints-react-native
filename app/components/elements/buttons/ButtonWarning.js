'use strict';

const buttonWarningFactory = function ({React}) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const ButtonBase = require('./ButtonBase')({React});

    const {
        string,
        func
        } = React.PropTypes;

    return function ButtonWarning(props) {
        ButtonWarning.propTypes = {
            ref: string,
            onPress: func,
            text: string
        };

        return {
            render: function render() {
                return <ButtonBase
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    text={this.props.text}
                    secondaryStyle={CommonStyles.btnWarning}
                    secondaryStyleText={CommonStyles.btnWarningText}/>;

            }
        };
    };
};

export default buttonWarningFactory;
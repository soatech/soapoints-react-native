'use strict';

const buttonDangerFactory = function ({React}) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const ButtonBase = require('./ButtonBase')({React});

    const {
        string,
        func
        } = React.PropTypes;

    return function ButtonDanger(props) {
        ButtonDanger.propTypes = {
            ref: string,
            onPress: func,
            text: string
        };

        return {
            render: function render() {
                return <ButtonBase
                    ref={this.props.ref}
                    onPress={this.props.onPress}
                    text={this.props.text}
                    secondaryStyle={CommonStyles.btnDanger}
                    secondaryStyleText={CommonStyles.btnDangerText}/>;

            }
        };
    };
};

export default buttonDangerFactory;
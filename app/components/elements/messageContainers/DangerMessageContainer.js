'use strict';

const dangerMessageContainerFactory = function ({ React }) {
    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const BaseMessageContainer = require('./BaseMessageContainer')({React});

    const {
        string
        } = React.PropTypes;

    return function DangerMessageContainer(props) {
        DangerMessageContainer.propTypes = {
            message: string
        };

        return {
            render: function render() {
                return <BaseMessageContainer message={this.props.message}
                                             secondaryStyle={CommonStyles.btnDanger}
                                             secondaryStyleText={CommonStyles.btnDangerText}/>;
            }
        };
    };
};

export default dangerMessageContainerFactory;
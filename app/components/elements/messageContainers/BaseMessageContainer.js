'use strict';

const baseMessageContainerFactory = function ({ React }) {
    const {
        View,
        Text
        } = React;

    const {
        CommonStyles
        } = require('../../../styles/Styles');

    const {
        string,
        object
        } = React.PropTypes;

    return function BaseMessageContainer(props) {
        BaseMessageContainer.propTypes = {
            secondaryStyle: object,
            secondaryStyleText: object,
            message: string
        };

        return {
            render: function render() {
                return <View style={[CommonStyles.btnAll, this.props.secondaryStyle]}>
                    <Text style={[CommonStyles.btnAllText, this.props.secondaryStyleText]}>{this.props.message}</Text>
                </View>;
            }
        };
    };
};

export default baseMessageContainerFactory;
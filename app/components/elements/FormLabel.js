'use strict';

const formLabelFactory = function ({React}) {
    const {
        View,
        Text
        } = React;

    const {CommonStyles} = require('../../styles/Styles');

    const {
        string
        } = React.PropTypes;

    return function FormLabel(props) {
        FormLabel.propTypes = {
            text: string
        };

        return {
            render: function render() {
                // TODO: this should probably pull from children as well, like how Text does
                return <View>
                    <Text style={{textAlign: 'left', width: 300}}>{this.props.text}</Text>
                </View>;
            }
        };
    };
};

export default formLabelFactory;
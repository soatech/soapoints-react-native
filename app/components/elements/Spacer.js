'use strict';

const spacerFactory = function ({React}) {
    const {
        View
        } = React;

    const {
        CommonStyles
        } = require('../../styles/Styles');

    const {
        number
        } = React.PropTypes;

    return function Spacer(props) {
        Spacer.propTypes = {
            height: number
        };

        return {
            render: function render() {
                let style = {};

                if (this.props.height) {
                    style.height = this.props.height;
                }

                return <View style={[CommonStyles.spacer, style]}/>;
            }
        };
    };
};

export default spacerFactory;
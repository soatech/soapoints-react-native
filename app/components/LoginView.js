'use strict';
const objectAssign = require('object-assign');

const loginViewFactory = function ({ React }) {
    const {
        View,
        TextInput
        } = React;

    const {
        FormLabel,
        Spacer,
        DangerMessageContainer,
        ButtonDefault,
        ButtonPrimary
        } = require('../components/Elements')({React});

    const {
        CommonStyles
        } = require('../styles/Styles');

    const Actions = require('../enums/Actions');
    const ErrorHandler = require('../util/ErrorHandlerMixin');

    const userServiceFactory = require('../services/UserService');

    const userFactory = require('../models/User');

    const {
        object
        } = React.PropTypes;

    return function LoginView(props) {
        LoginView.propTypes = {
            dispatcher: object
        };

        let userService:UserService = userServiceFactory({dispatcher: props.dispatcher});
        let user:User = userFactory({user: {}});
        let dispatcher:Dispatcher = props.dispatcher;

        let loginSuccessHandler = function loginSuccessHandler(payload:Object) {
            reactObj.setState({
                error: undefined
            }, () => dispatcher.dispatch(Actions.navigation.GOTO_SHARED_TASKS, {authenticatedUser: payload}));
        };

        let loginFailureHandler = function loginFailureHandler(payload:Object) {
            // TODO: Eric Elliott says setState and forceUpdate should be avoided and you should re-render manually
            // But I have no idea how to trigger a re-render without going to the very parent
            // Which is possible, but would have to pass a reference all the way down or use the
            // dispatcher.  Then having the parent pass down the error all through the chain
            reactObj.setState({error: reactObj.buildError(payload)});
        };

        let registerBtn_pressHandler = function registerBtn_pressHandler() {
            dispatcher.dispatch(Actions.navigation.GOTO_REGISTRATION, {});
        };

        let username_onChangeHandler = function username_onChangeHandler(event) {
            user.username = event.nativeEvent.text;
        };

        let username_onSubmitEditingHandler = function username_onSubmitEditingHandler(event) {
            username_onChangeHandler(event);
            reactObj.refs.PasswordInput.focus();
        };

        let password_onChangeHandler = function password_onChangeHandler(event) {
            user.password = event.nativeEvent.text;
        };

        let password_onSubmitEditingHandler = function password_onSubmitEditingHandler(event) {
            password_onChangeHandler(event);

            if (user.username && user.username.length) {
                loginBtn_pressHandler();
            }
        };

        let loginBtn_pressHandler = function loginBtn_pressHandler() {
            userService.login(user);
        };

        let reactObj = objectAssign({}, React.Component.prototype, ErrorHandler, {
            state: {
                error: undefined
            },
            componentDidMount: function componentDidMount() {
                // register actions
                this.props.dispatcher.register(
                    Actions.user.LOGIN_SUCCESS,
                    `LoginView-${Actions.user.LOGIN_SUCCESS}`,
                    (payload) => loginSuccessHandler(payload));

                this.props.dispatcher.register(
                    Actions.user.LOGIN_FAILURE,
                    `LoginView-${Actions.user.LOGIN_FAILURE}`,
                    (payload) => loginFailureHandler(payload));
            },
            componentWillUnmount: function componentWillUnmount() {
                // unregister actions
                this.props.dispatcher.unregister(Actions.user.LOGIN_SUCCESS, `LoginView-${Actions.user.LOGIN_SUCCESS}`);
                this.props.dispatcher.unregister(Actions.user.LOGIN_FAILURE, `LoginView-${Actions.user.LOGIN_FAILURE}`);

                userService = null;
                user = null;
            },
            render: function render() {

                let errorMessage;

                if (this.state && this.state.error) {
                    errorMessage = <DangerMessageContainer message={this.state.error}/>;
                }

                return <View style={CommonStyles.container}>
                    {errorMessage}
                    <FormLabel text="Username:"/>
                    <View>
                        <TextInput ref="UsernameInput"
                                   autoFocus={true}
                                   placeholder="Username"
                                   style={CommonStyles.formInput}
                                   onChange={(event) => username_onChangeHandler(event)}
                                   onSubmitEditing={(event) => username_onSubmitEditingHandler(event)}/>
                    </View>
                    <Spacer/>
                    <FormLabel text="Password:"/>
                    <View>
                        <TextInput ref="PasswordInput"
                                   placeholder="******"
                                   secureTextEntry={true}
                                   style={CommonStyles.formInput}
                                   onChange={(event) => password_onChangeHandler(event)}
                                   onSubmitEditing={(event) => password_onSubmitEditingHandler(event)}/>
                    </View>
                    <Spacer/>
                    <ButtonPrimary
                        ref="LoginButton"
                        onPress={(event) => loginBtn_pressHandler()}
                        text="Login"/>
                    <Spacer height={20}/>
                    <FormLabel text="Don't have an account?"/>
                    <ButtonDefault
                        ref="RegisterButton"
                        onPress={(event) => registerBtn_pressHandler()}
                        text="Register"/>
                </View>;
            }
        });

        return reactObj;
    };
};

export default loginViewFactory;
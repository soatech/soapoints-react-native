'use strict';
const objectAssign = require('object-assign');

const registerViewFactory = function ({ React }) {
    const {
        View,
        TextInput
        } = React;

    const {
        FormLabel,
        Spacer,
        DangerMessageContainer,
        ButtonDefault,
        ButtonPrimary
        } = require('../components/Elements')({React});

    const {
        CommonStyles
        } = require('../styles/Styles');

    const Actions = require('../enums/Actions');
    const userServiceFactory = require('../services/UserService');
    const userFactory = require('../models/User');

    const {
        object
        } = React.PropTypes;

    return function RegisterView(props) {
        RegisterView.propTypes = {
            dispatcher: object
        };

        let userService:UserService = userServiceFactory({dispatcher: props.dispatcher});
        let dispatcher:Dispatcher = props.dispatcher;
        let user = userFactory({user: {}});

        let registerSuccessHandler = function registerSuccessHandler(payload) {
            reactObj.setState({
                error: undefined
            }, () => dispatcher.dispatch(Actions.navigation.GOTO_SHARED_TASKS, {authenticatedUser: payload}));
        };

        let registerFailureHandler = function registerFailureHandler(payload) {
            reactObj.setState({error: (payload.hasOwnProperty('error') ? payload.error : "System Error")});
        };

        let generic_onChangeHandler = function generic_onChangeHandler(event, field) {
            let obj = {};
            obj[field] = event.nativeEvent.text;

            user.setAll(obj);
        };

        let generic_onSubmitEditingHandler = function generic_onSubmitEditingHandler(event, field, nextComp) {
            generic_onChangeHandler(event, field);

            if (nextComp) {
                nextComp.focus();
            }
        };

        let validateUser = function validateUser(callback:Function) {
            if (!user.username || user.username.length <= 1) {
                reactObj.setState({
                    error: "Username is required."
                });

                return;
            }

            if (!user.password || user.password.length <= 1) {
                reactObj.setState({
                    error: "Password is required."
                });

                return;
            }

            callback();
        };

        let password_onSubmitEditingHandler = function password_onSubmitEditingHandler(event) {
            validateUser(() => registerBtn_pressHandler());
        };

        let registerBtn_pressHandler = function registerBtn_pressHandler() {
            userService.register(user);
        };

        let cancelBtn_pressHandler = function cancelBtn_pressHandler() {
            reactObj.setState({
                error: undefined
            }, () => dispatcher.dispatch(Actions.navigation.GO_BACK, {}));
        };

        let reactObj = objectAssign({}, React.Component.prototype, {
            componentDidMount: function omponentDidMount() {
                // register actions
                this.props.dispatcher.register(
                    Actions.user.REGISTER_SUCCESS,
                    `RegisterView-${Actions.user.REGISTER_SUCCESS}`,
                    (payload) => registerSuccessHandler(payload));

                this.props.dispatcher.register(
                    Actions.user.REGISTER_FAILURE,
                    `RegisterView-${Actions.user.REGISTER_FAILURE}`,
                    (payload) => registerFailureHandler(payload));
            },
            componentWillUnmount: function componentWillUnmount() {
                // register actions
                this.props.dispatcher.unregister(Actions.user.REGISTER_SUCCESS, `RegisterView-${Actions.user.REGISTER_SUCCESS}`);
                this.props.dispatcher.unregister(Actions.user.REGISTER_FAILURE, `RegisterView-${Actions.user.REGISTER_FAILURE}`);

                userService = null;
                user = null;
            },
            render: function render() {
                let errorMessage;

                if (this.state && this.state.error) {
                    errorMessage = <DangerMessageContainer message={this.state.error}/>;
                }

                return <View style={CommonStyles.container}>
                    {errorMessage}
                    <FormLabel text="First Name:"/>
                    <View>
                        <TextInput ref="FirstNameInput"
                                   autoFocus={true}
                                   placeholder="First Name"
                                   style={CommonStyles.formInput}
                                   onChange={(event) => generic_onChangeHandler(event, 'firstName')}
                                   onSubmitEditing={(event) => generic_onSubmitEditingHandler(event, 'firstName', this.refs.LastNameInput)}/>
                    </View>
                    <Spacer/>
                    <FormLabel text="Last Name:"/>
                    <View>
                        <TextInput ref="LastNameInput"
                                   placeholder="Last Name"
                                   style={CommonStyles.formInput}
                                   onChange={(event) => generic_onChangeHandler(event, 'lastName')}
                                   onSubmitEditing={(event) => generic_onSubmitEditingHandler(event, 'lastName', this.refs.EmailInput)}/>
                    </View>
                    <Spacer/>
                    <FormLabel text="Email:"/>
                    <View>
                        <TextInput ref="EmailInput"
                                   placeholder="email@example.com"
                                   style={CommonStyles.formInput}
                                   keyboardType="email-address"
                                   onChange={(event) => generic_onChangeHandler(event, 'email')}
                                   onSubmitEditing={(event) => generic_onSubmitEditingHandler(event, 'email', this.refs.UsernameInput)}/>
                    </View>
                    <Spacer/>
                    <FormLabel text="Username:"/>
                    <View>
                        <TextInput ref="UsernameInput"
                                   placeholder="Username"
                                   style={CommonStyles.formInput}
                                   onChange={(event) => generic_onChangeHandler(event, 'username')}
                                   onSubmitEditing={(event) => generic_onSubmitEditingHandler(event, 'username', this.refs.PasswordInput)}/>
                    </View>
                    <Spacer/>
                    <FormLabel text="Password:"/>
                    <View>
                        <TextInput ref="PasswordInput"
                                   placeholder="******"
                                   secureTextEntry={true}
                                   style={CommonStyles.formInput}
                                   onChange={(event) => generic_onChangeHandler(event, 'password')}
                                   onSubmitEditing={(event) => password_onSubmitEditingHandler()}/>
                    </View>
                    <Spacer/>
                    <ButtonPrimary
                        ref="RegisterBtn"
                        onPress={(event) => registerBtn_pressHandler()}
                        text="Register"/>
                    <Spacer/>
                    <ButtonDefault
                        ref="CancelBtn"
                        onPress={(event) => cancelBtn_pressHandler()}
                        text="Cancel"/>
                </View>;
            }
        });

        return reactObj;
    };
};

export default registerViewFactory;
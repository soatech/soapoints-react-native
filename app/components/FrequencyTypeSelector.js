'use strict';


const frequencyTypeSelectorFactory = function ({ React }) {

    const objectAssign = require('object-assign');
    const _ = require('lodash');

    const {
        View,
        ListView
        } = React;

    const {
        ButtonDefault
        } = require('../components/Elements')({React});

    const {
        CommonStyles
        } = require('../styles/Styles');

    const Actions = require('../enums/Actions');

    const FrequencyTypes = require('../enums/FrequencyTypes');

    const {
        object
        } = React.PropTypes;

    return function FrequencyTypeSelector(props) {
        FrequencyTypeSelector.propTypes = {
            dispatcher: object
        };

        let dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        let frequencyType_pressHandler = function frequencyType_pressHandler(rowData:Object) {
            console.log("frequencyType_pressHandler");
            console.log(rowData);
            props.dispatcher.dispatch(Actions.navigation.RETURN_FREQUENCY_TYPE_SELECTOR, {selectedFrequencyType: rowData});
        };

        let cancelPressHandler = function cancelPressHandler(event) {
            props.dispatcher.dispatch(Actions.navigation.RETURN_FREQUENCY_TYPE_SELECTOR, {});
        };

        let reactObj = objectAssign({}, React.Component.prototype, {
            state: {
                frequencyTypes: dataSource.cloneWithRows(_.values(FrequencyTypes))
            },
            render: function render() {
                return <View style={CommonStyles.container}>
                    <ListView
                        dataSource={this.state.frequencyTypes}
                        renderRow={(rowData) => <ButtonDefault
                        onPress={(event) => frequencyType_pressHandler(rowData)}
                        text={rowData}/>}/>
                    <ButtonDefault
                        ref="CancelButton"
                        onPress={(event) => cancelPressHandler(event)}
                        text="Cancel"/>
                </View>;
            }
        });

        return reactObj;
    };
};

export default frequencyTypeSelectorFactory;
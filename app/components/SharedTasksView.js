'use strict';

const objectAssign = require('object-assign');

const sharedTasksViewFactory = function ({ React }) {
    const {
        View,
        Text,
        ListView,
        } = React;

    const {
        Spacer,
        DangerMessageContainer,
        ButtonDefault,
        ButtonPrimary,
        ButtonSuccess
        } = require('../components/Elements')({React});

    const {
        CommonStyles
        } = require('../styles/Styles');

    const Actions = require('../enums/Actions');

    const taskServiceFactory = require('../services/TaskService');
    const FrequencyTypes = require('../enums/FrequencyTypes');
    const ErrorHandler = require('../util/ErrorHandlerMixin');

    const {
        object,
        string
        } = React.PropTypes;

    return function SharedTasksView(props) {
        SharedTasksView.propTypes = {
            dispatcher: object,
            authenticatedUser: object,
            selectedFrequencyType: string
        };

        let taskService = taskServiceFactory({dispatcher: props.dispatcher, authToken: props.authenticatedUser.token});
        let dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        let listSuccessHandler = function listSuccessHandler(payload:Object) {
            console.log("SharedTasksView:72");
            console.log(payload);
            reactObj.setState({
                error: null,
                tasks: dataSource.cloneWithRows(payload)
            });
        };

        let listFailureHandler = function listFailureHandler(payload) {
            console.log("SharedTasksView:81");
            console.log(payload);
            reactObj.setState({
                tasks: null,
                error: reactObj.buildError(payload)
            });
        };

        let addButton_pressHandler = function addButton_pressHandler(event) {
            // Go to add/edit page
        };

        let editButton_pressHandler = function editButton_pressHandler(rowData:Object) {
            // go to edit page with select task
        };

        let frequencyTypeSelectorButton_onPressHandler = function frequencyTypeSelectorButton_onPressHandler(event) {
            props.dispatcher.dispatch(Actions.navigation.GOTO_FREQUENCY_TYPE_SELECTOR);
        };

        let renderTaskRow = function renderTaskRow(rowData:Object) {
            return <ButtonDefault
                onPress={(event) => editButton_pressHandler(rowData)}
                text={rowData.name}/>;
        };

        let reactObj = objectAssign({}, React.Component.prototype, ErrorHandler, {
            state: {
                error: undefined,
                tasks: dataSource.cloneWithRows([]),
                frequencyType: (props.selectedFrequencyType) ? props.selectedFrequencyType : FrequencyTypes.DAILY
            },
            componentDidMount: function componentDidMount() {
                this.props.dispatcher.register(
                    Actions.task.LIST_SHARED_SUCCESS,
                    `SharedTasksView-${Actions.task.LIST_SHARED_SUCCESS}`,
                    (payload) => listSuccessHandler(payload));

                this.props.dispatcher.register(
                    Actions.task.LIST_SHARED_FAILURE,
                    `SharedTasksView-${Actions.task.LIST_SHARED_FAILURE}`,
                    (payload) => listFailureHandler(payload));

                taskService.listSharedTasks(this.state.frequencyType);
            },
            componentWillUnmount: function componentWillUnmount() {
                console.log("componentWillUnmount");
                this.props.dispatcher.unregister(
                    Actions.task.LIST_SHARED_SUCCESS,
                    `SharedTasksView-${Actions.task.LIST_SHARED_SUCCESS}`);

                this.props.dispatcher.unregister(
                    Actions.task.LIST_SHARED_FAILURE,
                    `SharedTasksView-${Actions.task.LIST_SHARED_FAILURE}`);

                taskService = null;
                dataSource = null;
            },
            render: function render() {
                let errorMessage;

                if (this.state.error) {
                    errorMessage = <DangerMessageContainer message={this.state.error}/>;
                }

                return <View style={CommonStyles.container}>
                    {errorMessage}
                    <Text>My Shared Tasks</Text>
                    <ButtonSuccess
                        ref="FrequencyTypeSelectorButton"
                        onPress={(event) => frequencyTypeSelectorButton_onPressHandler(event)}
                        text={this.state.frequencyType}/>
                    <Spacer/>
                    <ListView
                        dataSource={this.state.tasks}
                        renderRow={(rowData) => renderTaskRow(rowData)}
                    />
                    <ButtonPrimary ref="AddButton" onPress={(event) => addButton_pressHandler} text="Add Task"/>
                </View>;
            }
        });

        return reactObj;
    };
};

export default sharedTasksViewFactory;
'use strict';

const elementsFactory = function ({ React }) {
    return {
        // Form Items
        FormLabel: require('./elements/FormLabel')({React}),

        // Buttons
        ButtonDefault: require('./elements/buttons/ButtonDefault')({React}),
        ButtonPrimary: require('./elements/buttons/ButtonPrimary')({React}),
        ButtonSuccess: require('./elements/buttons/ButtonSuccess')({React}),
        ButtonDanger: require('./elements/buttons/ButtonDanger')({React}),
        ButtonInfo: require('./elements/buttons/ButtonInfo')({React}),
        ButtonWarning: require('./elements/buttons/ButtonWarning')({React}),

        // Containers
        DangerMessageContainer: require('./elements/messageContainers/DangerMessageContainer')({React}),

        // Misc
        Spacer: require('./elements/Spacer')({React})
    }
};

export default elementsFactory;
'use strict';

const soaPointsReactNativeFactory = require('./app/SoaPointsNative');

const React = require('react-native');
const {
    AppRegistry
    } = React;

const SoaPointsReactNative = soaPointsReactNativeFactory({React});

console.log(SoaPointsReactNative);

AppRegistry.registerComponent('SoapointsReactNative', () => SoaPointsReactNative);
